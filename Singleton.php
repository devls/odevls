<?php
abstract class ODevls_Singleton {

    private static $instances = array();

    /**
     * Gets instance
     *
     * @return static
     */
    public static function getInstance()
    {
        $class = get_called_class();

        if( !isset( self::$instances[$class] ) ) {
            $args = func_get_args();
            if( !empty( $args ) ) {
                $reflector = new ReflectionClass( $class );
                $instance = $reflector->newInstanceArgs( $args );
            } else {
                $instance = new static();
            }

            return self::$instances[$class] = $instance;
        }

        return self::$instances[$class];
    }

}