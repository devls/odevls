<?php
class ODevls_Table {

    /* @var array $rows */
    private $rows = array();

    /* @var array $columns */
    private $columns = array();

    /* @var array $columnMappings */
    private $columnMappings = array();

    /* @var array $tableClasses */
    private $tableClasses = array();

    /* @var boolean $disableHeader */
    private $disableHeader = false;

    public function __construct( array $classes = array( 'table', 'table-hover', 'table-condensed' ) )
    {
        $this->tableClasses = $classes;
    }

    /**
     * Columns. Expects the machine readable column ids as values
     *
     * @param array $columns
     */
    public function addColumns( array $columns )
    {
        $columns = array_values( $columns );
        $this->columns[] = $columns;
    }

    public function disableHeader()
    {
        $this->disableHeader = true;
    }

    public function addRows( array $array )
    {
        foreach( $array as $key => $row ) {
            $this->addRow($row, $key);
        }
    }

    public function addRow($row, $key = null)
    {
        if( is_array( $row ) ) {
            $row = new ODevls_Table_Row( $row );
        } else if( ! $row instanceof ODevls_Table_Row ) {
            throw new \Exception( "Invalid data to create Table" );
        }
        if( $key === null ) {
            $this->rows[] = $row;
        } else {
            $this->rows[ $key ] = $row;
        }

        $this->columns = $this->columns + $row->getColumns();

        return $this;
    }

    public function nameColumns( array $columnNames )
    {
        foreach( $columnNames as $machine => $human ) {
            $this->nameColumn( $machine, $human );
        }
    }

    public function nameColumn( $machine, $human )
    {
        $this->columnMappings[ $machine ] = $human;
    }

    public function __toString()
    {
        $output = <<<HTML
<table class="@{#classes}">
    <thead>
        <headings />
    </thead>
    <tbody>
        <rows />
    </tbody>
</table>
HTML;

        //Classes
        $output = str_replace( '@{#classes}', implode( ' ', $this->tableClasses ), $output );

        $th = <<<HTML
<th>@{#human-name}</th>
HTML;

        $headings = '';
        //Column Headings
        foreach( $this->columns as $column ) {
            if( isset( $this->columnMappings[ $column ] ) ) {
                $column = $this->columnMappings[ $column ];
            } else {
                $column = ucfirst( $column );
            }
            $headings .= str_replace( '@{#human-name}', $column, $th );
        }
        $output = str_replace( '<headings />', $headings, $output );

        $tbody = <<<HTML
<tbody>@{#rows}</tbody>
HTML;

        $tr = <<<HTML
<tr>@{#row}</tr>
HTML;

        //Rows
        $rows = '';
        foreach( $this->rows as $row ) {
            /* @var $row ODevls_Table_Row */
            $row = $row->getRow();
            $part = '';
            foreach( $this->columns as $column ) {
                if( !isset( $row[$column] ) ) {
                    $part .= '<td></td>';
                } else {
                    $part .= $row[$column];
                }
            }
            $rows .= str_replace( '@{#row}', $part, $tr );
        }
        $rows = str_replace( '@{#rows}', $rows, $tbody );
        $output = str_replace( '<rows />', $rows, $output );

        return $output;
    }

}