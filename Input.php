<?php
class ODevls_Input extends ODevls_Singleton {

    /* @var array */
    private $get;

    /* @var array */
    private $post;

    /* @var array */
    private $headers;

    /**
     * @param array $get
     * @param array $post
     * @param array $headers
     */
    public function __construct( array $get=null, array $post=null, array $headers=null )
    {
        $this->get = ( $get !== null ) ? $get : $_GET;
        $this->post = ( $post !== null ) ? $post : $_POST;
        $this->headers = ( $headers !== null ) ? $headers : self::getHeaders();
    }

    /**
     * Get request headers
     *
     * @return array
     */
    private static function getHeaders()
    {
        if ( function_exists('getallheaders') ) {
            return getallheaders();
        }

        //NGINX
        $headers = array();
        foreach ($_SERVER as $name => $value)
        {
            if (substr($name, 0, 5) == 'HTTP_')
            {
                $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
            }
        }

        return $headers;
    }

    /**
     * Get get param
     *
     * @param string $key
     * @param mixed $default
     * @return string|mixed
     */
    public function get( $key, $default=false )
    {
        return isset( $this->get[$key] ) ? $this->get[$key] : $default;
    }

    /**
     * Get post param
     *
     * @param string $key
     * @param mixed $default
     * @return string|mixed
     */
    public function post( $key, $default=false )
    {
        return isset( $this->post[$key] ) ? $this->post[$key] : $default;
    }

    /**
     * Get get param 1st priority post param 2nd priority
     *
     * @param string $key
     * @param mixed $default
     * @return string|mixed
     */
    public function getOrPost( $key, $default=false )
    {
        if( isset( $this->get[$key] ) ) {
            return $this->get[$key];
        } else if( isset( $this->post[$key] ) ) {
            return $this->post[$key];
        }

        return $default;
    }

    /**
     * Get post param 1st priority get param 2nd priority
     *
     * @param string $key
     * @param mixed $default
     * @return string|mixed
     */
    public function postOrGet( $key, $default=false )
    {
        if( isset( $this->post[$key] ) ) {
            return $this->post[$key];
        } else if( isset( $this->get[$key] ) ) {
            return $this->get[$key];
        }

        return $default;
    }

    /**
     * Get a request header by key
     *
     * @param string $key
     * @param mixed $default
     * @return string|mixed
     */
    public function header( $key, $default=false )
    {
        return isset( $this->headers[$key] ) ? $this->headers[$key] : $default;
    }

}