<?php
class ODevls_Table_Row {

    const TABLE_ROW_TD = <<<HTML
<td>@{#value}</td>
HTML;

    /* @var $row */
    private $row;

    public function __construct( array $row )
    {
        $this->row = $row;
    }

    /**
     * Gets the column names in this row.
     *
     * @return array
     */
    public function getColumns()
    {
        $columns = array();
        foreach( $this->row as $column => $value ) {
            $columns[] = $column;
        }
        return $columns;
    }

    /**
     * Gets the row formatted.
     *
     * @return array
     */
    public function getRow()
    {
        $row = $this->row;
        foreach( $row as &$part ) {
            $part = str_replace( '@{#value}', $part, self::TABLE_ROW_TD );
        } unset( $part );

        return $row;
    }

    /**
     * @return string
     */
    public static function getEmptyRow()
    {
        return str_replace( '@{#value}', '', self::TABLE_ROW_TD );
    }

}